package org.example.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Dto для обновления адреса Employee по его Id")
public class EmployeeUpdateAddressDto {
    @Schema(description = "Адрес на который нужно обновить")
    private String address;
    @Schema(description = "id пользователя")
    private Long id;
}
