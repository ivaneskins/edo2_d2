package org.example.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.EmployeeDto;
import org.example.feign.EmployeeFeignClient;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Employee")
public class EmployeeController {
    private final EmployeeFeignClient employeeFeignClient;

    @GetMapping("/getuser")
    EmployeeDto getByUsername(@RequestParam String username) {
        System.out.println("hello from main");
        return employeeFeignClient.getByUsername(username);
    }

}
